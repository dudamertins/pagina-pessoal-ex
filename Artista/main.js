document.addEventListener("DOMContentLoaded", function(event) {
    var btn1 = document.querySelector('.gold');
    btn1.onclick = function(){
        document.body.style.backgroundColor= 'gold';
    };
    var btn2 = document.querySelector('.beige');
    btn2.onclick = function(){
        document.body.style.backgroundColor= 'beige';
    };
    var btn3 = document.querySelector('.pink');
    btn3.onclick = function(){
        document.body.style.backgroundColor= 'pink';
    };
});
